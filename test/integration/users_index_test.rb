require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest
  def setup
    # @user = users(:michael)
    @admin = users(:michael)
    @non_admin = users(:archer)
    #@noactivated = users(:notactivated)
  end

  # test "index including pagination" do
  #   log_in_as(@user)
  #   get users_path
  #   assert_template 'users/index'
  #   assert_select 'div.pagination',count:2
  #   User.paginate(page: 1).each do |user|
  #     assert_select 'a[href=?]', user_path(user),text:user.name
  #   end
  # end

  test "index as admin including pagination and delete links" do
    log_in_as(@admin)
    get users_path
    assert_template 'users/index'
    #assert_select 'div.pagination'
    #first_page_of_users = User.paginate(page:1)
    first_page_of_users = User.where(activated:true).paginate(page:1)
    first_page_of_users.each do |user|
      assert_select 'a[href=?]',user_path(user),text:user.name
      unless user == @admin
        assert_select 'a[href=?]',user_path(user),text:'delete'
      end
    end
    assert_difference 'User.count',-1 do
      delete user_path(@non_admin)
    end
  end

  test "index as non-admin" do
    log_in_as(@non_admin)
    get users_path
    assert_select 'a',text:'delete',count:0
  end

  test "index as non-activated" do
    get signup_path
    post users_path, params:{user:{name:"Example User",
                                    email: "user@example.com",
                                    password: "password",
                                    password_confirmation: "password"}}
    user = assigns(:user)
    get user_path(user)
    assert_redirected_to root_url
  end

  test "index dont show activated user" do
    log_in_as(@admin)
    get users_path
    # user = User.find_by(name: "User 1")
    # p user
    # assert_not 'a[href=?]',user_path(user)

    user = User.where(activated:true).paginate(page:1)
    user.each do |use|
    # user.each do |use|
    #   #p use
      assert use.activated?
    end
      #i += 1
  #  end
  end

end
